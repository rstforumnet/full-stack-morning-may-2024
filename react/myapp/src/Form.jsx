import { useRef, useState } from 'react';

const Form = () => {
	console.log('I WAS JUST CALLED!!');
	const usernameRef = useRef();
	const passwordRef = useRef();
	let [error, setError] = useState(null);

	const handleSubmit = (event) => {
		event.preventDefault();
		// console.dir(event.target[0].value);
		// console.dir(event.target[1].value);
		// const { username, password } = event.target.elements;
		// console.dir(username.value);
		// console.dir(password.value);
		console.log(usernameRef.current.value);
		console.log(passwordRef.current.value);
	};

	const handleUsernameValidation = (e) => {
		if (e.target.value !== e.target.value.toLowerCase()) {
			setError('Username cannot contain capital letters');
		} else {
			setError(null);
		}
	};

	// const handleErrorChange = () => {
	// 	setError(Math.random());
	// };

	return (
		<div>
			{/* <h1>{error}</h1>
			<button onClick={handleErrorChange}>CHANGE</button> */}
			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor='username'>Username</label>
					<br />
					<input
						onChange={handleUsernameValidation}
						type='text'
						id='username'
						ref={usernameRef}
					/>
					<br />
					<span style={{ color: 'red', fontWeight: 'bold' }}>{error}</span>
				</div>
				<div>
					<label htmlFor='password'>Password</label>
					<br />
					<input type='password' id='password' ref={passwordRef} />
				</div>
				<button>Submit</button>
			</form>
		</div>
	);
};

export default Form;
