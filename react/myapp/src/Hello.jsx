export const Hello2 = () => {
	return <h1>Hello World</h1>;
};

export const HelloWorld = () => {
	return (
		<section>
			<h2>Hello Again, Hello Something</h2>
		</section>
	);
};

export default Hello2;

// export default Hello;
