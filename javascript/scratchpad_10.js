// // // function add(a, b) {
// // // 	return a + b;
// // // }

// // // // console.log(add(10, 20));

// // // const a = add;

// // // console.log(a(10, 20));

// // // function sqaure(num) {
// // // 	return num ** 2;
// // // }

// // // const cube = function (num) {
// // // 	return num ** 3;
// // // };

// // // console.log(sqaure(10));
// // // console.log(cube(10));

// // // const math = [
// // // 	function (a, b) {
// // // 		return a + b;
// // // 	},
// // // 	function (a, b) {
// // // 		return a - b;
// // // 	},
// // // 	function (a, b) {
// // // 		return a * b;
// // // 	},
// // // 	function (a, b) {
// // // 		return a / b;
// // // 	},
// // // ];

// // // console.log(math[3](10, 20));

// // // const math = {
// // // 	add: function (a, b) {
// // // 		return a + b;
// // // 	},
// // // 	sub: function (a, b) {
// // // 		return a - b;
// // // 	},
// // // 	mul: function (a, b) {
// // // 		return a * b;
// // // 	},
// // // 	div: function (a, b) {
// // // 		return a / b;
// // // 	},
// // // };

// // // console.log(math.add(10, 20));

// // function math(a, b, fn) {
// // 	return fn(a, b);
// // }

// // function add(a, b) {
// // 	return a + b;
// // }

// // console.log(math(50, 10, add));
// // console.log(
// // 	math(10, 20, function (a, b) {
// // 		return a * b;
// // 	})
// // );

// function randomPick(f1, f2) {
// 	let randNum = Math.random();
// 	if (randNum < 0.5) {
// 		f1();
// 	} else {
// 		f2();
// 	}
// }

// randomPick(
// 	function () {
// 		console.log('Hello World');
// 	},
// 	function () {
// 		console.log('Good bye');
// 	}
// );

// capitalize("hello world this is something")
// "Hello World This Is Something"

// function capitalize(sentence) {
// 	let words = sentence.split(' ');

// 	let capitalizedWords = [];

// 	for (let word of words) {
// 		capitalizedWords.push(word[0].toUpperCase() + word.slice(1));
// 	}

// 	return capitalizedWords.join(' ');
// }

// console.log(capitalize('hello world this is something'));

// function raiseBy(num) {
// 	return function (x) {
// 		return x ** num;
// 	};
// }

// const square = raiseBy(2);
// const cube = raiseBy(3);
// const hypercube = raiseBy(4);

// console.log(square(10));
// console.log(cube(10));
// console.log(hypercube(10));

function isBetween(x, y) {
	return function (num) {
		return num >= x && num <= y;
	};
}

const isUnderAge = isBetween(0, 18);
const isLegalAge = isBetween(21, 65);

console.log(isUnderAge(2));
