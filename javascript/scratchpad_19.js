// fetch('https://pokeapi.co/api/v2/pokemon')
// 	.then((res) => {
// 		return res.json();
// 	})
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log(err);
// 	});

// fetch(
// 	'https://pokeapi.co/api/v2/pokkljnszdcvjnklszdfjnklasdrfjnklsdfgzjnklsdfgjnklsdfjkgsndfljkgnsdlfkjgnsdklfgjnsdlfgjknlsdrfgkuemon'
// )
// 	.then((res) => {
// 		console.log('In the then block');
// 		if (!res.ok) {
// 			throw new Error();
// 		} else {
// 			return res.json();
// 		}
// 	})
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log('In the catch block');
// 		console.log(err);
// 	});

const root = document.querySelector('#root');

const section = document.createElement('section');
section.style.display = 'grid';
section.style.gridTemplateColumns = '1fr 1fr 1fr 1fr 1fr';
section.style.gap = '30px';

axios
	.get('https://pokeapi.co/api/v2/pokemon?limit=100000')
	.then((res) => {
		console.log('In the then block');
		for (let pokemon of res.data.results) {
			axios.get(pokemon.url).then((response) => {
				console.log(response.data);

				const card = document.createElement('article');
				card.style.display = 'flex';
				card.style.flexDirection = 'column';
				card.style.alignItems = 'center';
				card.style.justifyContent = 'center';

				const img = document.createElement('img');
				img.src = response.data.sprites.other['official-artwork'].front_default;
				img.style.width = '100%';

				const name = document.createElement('h3');
				name.innerText = response.data.name;
				name.style.textTransform = 'capitalize';

				card.append(img, name);
				section.append(card);
			});
		}
	})
	.catch((err) => {
		console.log('In the error block');
		console.log(err);
	});

root.append(section);
