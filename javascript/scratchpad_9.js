// function flipCoin() {
// 	let randomNum1 = Math.random();
// 	if (randomNum1 > 0.5) {
// 		console.log('HEADS');
// 	} else {
// 		console.log('TAILS');
// 	}
// }

// for (let i = 0; i < 50; i++) {
// 	flipCoin();
// }

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice() {
// 	rollDie();
// 	rollDie();
// 	rollDie();
// }

// throwDice();

// function greet(firstName, lastName, age) {
// 	console.log(
// 		`Hello, my name is ${firstName} ${lastName} and I am ${age} years old.`
// 	);
// }

// greet('John', 'Doe', 20);
// greet('Jane', 'Smith', 25);

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice(times) {
// 	for (let i = 0; i < times; i++) {
// 		rollDie();
// 	}
// }

// throwDice(30);

// function add(a, b) {
// 	return a + b;
// }

// console.log(add(10, 50));

// // let result = add(10, 20);
// // console.log(result);

// let fullName = 'John Doe';

// function greet() {
// 	let fullName = 'Jane Doe';
// 	console.log(fullName);
// }

// greet();
// console.log(fullName);

// var fullName = 'John Doe';

// if (true) {
// 	var fullName = 'Jane Smith';
// 	console.log(fullName);
// }

// console.log(fullName);

// for (var i = 0; i < 10; i++) {
// 	console.log(i);
// }

// console.log(`OUTSIDE THE LOOP: ${i}`);
const fullName = 'John Doe';

function outer() {
	function inner() {
		// let fullName = 'Jane Smith';
		console.log(fullName);
	}

	inner();
	console.log(fullName);
}

outer();
