// let day = 'hello';

// // if (day === 1) {
// // 	console.log('Mon');
// // } else if (day === 2) {
// // 	console.log('Tue');
// // }

// switch (day) {
// 	case 'hello':
// 		console.log('Mon');
// 		break;
// 	case 2:
// 		console.log('Tue');
// 		break;
// 	case 3:
// 		console.log('Wed');
// 		break;
// 	case 4:
// 		console.log('Thu');
// 		break;
// 	case 5:
// 		console.log('Fri');
// 		break;
// 	case 6:
// 		console.log('Sat');
// 		break;
// 	case 7:
// 		console.log('Sun');
// 		break;
// 	default:
// 		console.log('Invalid day code');
// }

// let currentStatus = 'online';

// if (currentStatus === 'online') {
// 	console.log('Green');
// } else {
// 	console.log('Red');
// }

// currentStatus === 'online' ? console.log('Green') : console.log('Red');

let currentStatus = 'offline';

let color =
	currentStatus === 'online'
		? 'green'
		: currentStatus === 'offline'
		? 'red'
		: 'yellow';

// let color;

// if (currentStatus === 'online') {
// 	color = 'green';
// } else if (currentStatus === 'offline') {
// 	color = 'red';
// } else {
// 	color = 'yellow';
// }

console.log(color);
