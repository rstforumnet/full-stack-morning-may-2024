function initializeDeck() {
	const deck = [];
	const suits = ['hearts', 'diamonds', 'spades', 'clubs'];
	const values = '2,3,4,5,6,7,8,9,10,J,Q,K,A';
	for (let value of values.split(',')) {
		for (let suit of suits) {
			deck.push({ value, suit });
		}
	}
	return deck;
}

function drawCard(deck, drawnCards) {
	const card = deck.pop();
	drawnCards.push(card);
	return card;
}

function drawMultiple(numCards, deck, drawnCards) {
	const cards = [];
	for (let i = 0; i < numCards; i++) {
		cards.push(drawCard(deck, drawnCards));
	}
	return cards;
}

function shuffle(deck) {
	// loop over array backwards
	for (let i = deck.length - 1; i > 0; i--) {
		// pick random index before current element
		let j = Math.floor(Math.random() * (i + 1));
		[deck[i], deck[j]] = [deck[j], deck[i]];
	}
	return deck;
}

const deck1 = initializeDeck();
const hand = [];

shuffle(deck1);

drawCard(deck1, hand);
drawCard(deck1, hand);
drawCard(deck1, hand);
drawMultiple(3, deck1, hand);

console.log(deck1);
console.log(hand);

function makeDeck() {
	return {
		deck: [],
		drawnCards: [],
		suits: ['hearts', 'diamonds', 'spades', 'clubs'],
		values: '2,3,4,5,6,7,8,9,10,J,Q,K,A',
		initializeDeck() {
			for (let value of this.values.split(',')) {
				for (let suit of this.suits) {
					this.deck.push({ value, suit });
				}
			}
			return this.deck;
		},
		drawCard() {
			const card = this.deck.pop();
			this.drawnCards.push(card);
			return card;
		},
		drawMultiple(numCards) {
			const cards = [];
			for (let i = 0; i < numCards; i++) {
				cards.push(this.drawCard());
			}
			return cards;
		},
		shuffle() {
			const { deck } = this;
			for (let i = deck.length - 1; i > 0; i--) {
				let j = Math.floor(Math.random() * (i + 1));
				[deck[i], deck[j]] = [deck[j], deck[i]];
			}
		},
	};
}

const deck2 = makeDeck();
deck2.initializeDeck();
deck2.shuffle();
deck2.drawCard();
deck2.drawCard();
deck2.drawCard();
deck2.drawMultiple(3);

const deck3 = makeDeck();

console.log(deck2);
console.log(deck3);
