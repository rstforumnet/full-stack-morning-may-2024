const taskInp = document.querySelector('#todo');
const button = document.querySelector('button');
const taskList = document.querySelector('#todos');

button.addEventListener('click', function () {
	if (!taskInp.value) {
		return;
	}

	const todo = document.createElement('li');
	todo.innerText = taskInp.value;

	todo.addEventListener('click', function () {
		todo.classList.toggle('done');
	});

	const delBtn = document.createElement('button');
	delBtn.innerText = 'Delete';
	delBtn.addEventListener('click', function () {
		todo.remove();
	});

	todo.append(delBtn);
	taskList.append(todo);
	taskInp.value = '';
});
