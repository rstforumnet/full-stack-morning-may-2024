// let age = 22;

// // if (age > 18) {
// // 	console.log('You are allowed to enter');
// // }

// if (age % 2 === 0) {
// 	console.log('Age is even');
// } else {
// 	console.log('Age is odd');
// }

// let age = 100;

// if (age >= 65) {
// 	console.log('Drinks are free');
// } else if (age >= 21) {
// 	console.log('You can enter and you can drink');
// } else if (age >= 18) {
// 	console.log('You can enter but you cannot drink');
// } else {
// 	console.log('You are not allowed.');
// }

// let age = 90;

// if (age >= 65) {
// 	console.log('You can enter but you cannot drink');
// } else if (age >= 21) {
// 	console.log('You can enter and you can drink');
// } else if (age >= 18) {
// 	console.log('Drinks are free');
// } else {
// 	console.log('You are not allowed.');
// }

// let password = 'helloworld@123';

// if (password.length >= 8) {
// 	if (password.indexOf(' ') !== -1) {
// 		console.log('Password cannot include spaces');
// 	} else {
// 		console.log('Valid password');
// 	}
// } else {
// 	console.log('Password is too short!');
// }

// let a = 10;
// console.log(a);

// let loggedInUser = 'jane';

// if (loggedInUser) {
// 	console.log('Welcome to the site');
// } else {
// 	console.log('You have to login to view the page');
// }

let age = 4;

if (age >= 18 && age < 21) {
	console.log('You can enter but you cannot drink');
} else if (age >= 21 && age < 65) {
	console.log('You can enter and you can drink');
} else if (age >= 65) {
	console.log('Drinks are free');
} else {
	console.log('You are not allowed.');
}
