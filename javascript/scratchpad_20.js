// function fetchData() {
// 	const response = axios.get('https://pokeapi.co/api/v2/pokemon');
// 	response
// 		.then((data) => {
// 			console.log(data);
// 		})
// 		.catch((err) => {
// 			console.log(err);
// 		});
// }

// fetchData();

// async function greet() {
// 	return 'Hello World';
// }

// // // function greet() {
// // // 	return new Promise((resolve, reject) => {
// // // 		reject('Hello World');
// // // 	});
// // // }

// // console.log(greet());

// greet().then((data) => console.log(data));

// async function add(x, y) {
// 	if (typeof x !== 'number' || typeof y !== 'number') {
// 		throw 'X and Y have to be numbers';
// 	}
// 	return x + y;
// }

// add(10, 20)
// 	.then((data) => console.log(data))
// 	.catch((err) => console.log(err));

// async function fetchData() {
// 	try {
// 		const response = await axios.get('https://pokeapi.co/api/v2/pokemon/1');
// 		console.log(response);
// 	} catch (err) {
// 		console.log(err);
// 	}
// }

// fetchData();

// async function fetchData() {
// 	try {
// 		const pokemon1 = await axios.get('https://pokeapi.co/api/v2/pokemon/1');
// 		const pokemon2 = await axios.get('https://pokeapi.co/api/v2/pokemon/2');
// 		const pokemon3 = await axios.get('https://pokeapi.co/api/v2/pokemon/3');

// 		console.log(pokemon1.data.name);
// 		console.log(pokemon2.data.name);
// 		console.log(pokemon3.data.name);
// 	} catch (err) {
// 		console.log(err);
// 	}
// }

// fetchData();

async function fetchData() {
	try {
		const pokemon1Promise = axios.get('https://pokeapi.co/api/v2/pokemon/1');
		const pokemon2Promise = axios.get('https://pokeapi.co/api/v2/pokemon/2');
		const pokemon3Promise = axios.get('https://pokeapi.co/api/v2/pokemon/3');

		// const poke1 = await pokemon1Promise;
		// const poke2 = await pokemon2Promise;
		// const poke3 = await pokemon3Promise;
		const results = await Promise.all([
			pokemon1Promise,
			pokemon2Promise,
			pokemon3Promise,
		]);

		// console.log(poke1);
		// console.log(poke2);
		// console.log(poke3);
		console.log(results);
	} catch (err) {
		console.log(err);
	}
}

fetchData();
