// function user(firstName, lastName, age) {
// 	return {
// 		firstName,
// 		lastName,
// 		age,
// 		greet() {
// 			return 'Hello World!';
// 		},
// 	};
// }

// // function User(firstName, lastName, age) {
// // 	this.firstName = firstName;
// // 	this.lastName = lastName;
// // 	this.age = age;
// // }
// // User.prototype.greet = function () {
// // 	return 'Hello World';
// // };

// class User {
// 	constructor(firstName, lastName, age) {
// 		this.firstName = firstName;
// 		this.lastName = lastName;
// 		this.age = age;
// 	}

// 	greet() {
// 		return 'Hello World';
// 	}
// }

// const john = user('John', 'Doe', 20);
// const jane = user('Jane', 'Smith', 25);
// const jack = {
// 	firstName: 'Jack',
// 	lastName: 'Smith',
// 	age: 30,
// 	greet() {
// 		return 'Hello World!';
// 	},
// };
// const james = new User('James', 'Bond', 80);
// const hames = new User('Hames', 'Kand', 100);

// console.log(john);
// console.log(jane);
// console.log(jack);
// console.log(james);
// console.log(hames);

// fib(10) => [0,1,1,2,3]
// 'silent' 'listen'

// numGroup([1,2,3,45,67,78,89,89,9,90,9], 3) // [[1,2,3], [45,67,78], [89, 89, 9], [90,9]]

// genRandomRGBColor() => '#fffa12'

// removeDups([1,2,2,3,34,5,6,3,23,3,34]) => [1,2,3,34,5,6,23,34]
