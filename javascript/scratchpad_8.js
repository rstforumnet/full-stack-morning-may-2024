// // for (let count = 0; count < 10; count++) {
// // 	console.log('Hello world!');
// // }

// // const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// // let total = 0;
// // for (let i = 0; i < nums.length; i++) {
// // 	total += nums[i];
// // }

// // // console.log(total);

// // const movies = [
// // 	{ movieName: 'Inception', rating: 3.8 },
// // 	{ movieName: 'Avengers', rating: 3.4 },
// // 	{ movieName: 'Iron Man', rating: 2.9 },
// // ];

// // for (let i = 0; i < movies.length; i++) {
// // 	let movie = movies[i];
// // 	console.log(`${movie.movieName} has a rating of ${movie.rating}`);
// // }

// // const word = 'Hello World';

// // // for (let i = 0; i < word.length; i++) {
// // // 	console.log(word[i]);
// // // }

// // let reversedWord = '';
// // for (let i = word.length - 1; i >= 0; i--) {
// // 	reversedWord += word[i];
// // }
// // console.log(reversedWord);

// // for (let i = 0; i < 5; i++) {
// // 	console.log(`${i} - OUTER LOOP`);

// // 	for (let j = 0; j < 5; j++) {
// // 		console.log(`    ${j} - INNER LOOP`);

// // 		for (let k = 0; k < 5; k++) {
// // 			console.log(`        ${k} - INNER KA INNER LOOP`);
// // 		}
// // 	}
// // }

// // const gameBoard = [
// // 	[4, 64, 8, 4],
// // 	[128, 32, 4, 16],
// // 	[16, 4, 4, 32],
// // 	[2, 16, 16, 2],
// // ];

// // let total = 0;

// // for (let i = 0; i < gameBoard.length; i++) {
// // 	for (let j = 0; j < gameBoard[i].length; j++) {
// // 		console.log(gameBoard[i][j]);
// // 		total += gameBoard[i][j];
// // 	}
// // }

// // console.log('TOTAL', total);

// // const gameBoard = [
// // 	[
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 	],
// // 	[
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 	],
// // 	[
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 	],
// // 	[
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 		[4, 64, 8, 4],
// // 	],
// // ];

// // for (let i = 0; i < 10; i++) {
// // 	console.log(i);
// // }

// // let i = 0;
// // while (i < 10) {
// // 	console.log(i);
// // 	i++;
// // }

// // let target = Math.floor(Math.random() * 10) + 1;
// // let guess = Math.floor(Math.random() * 10) + 1;

// // while (target !== guess) {
// // 	console.log(`TARGET: ${target} | GUESS: ${guess}`);
// // 	guess = Math.floor(Math.random() * 10) + 1;
// // }

// // console.log(`FINAL TARGET: ${target} | GUESS: ${guess}`);

// // let target = Math.floor(Math.random() * 10) + 1;
// // let guess = Math.floor(Math.random() * 10) + 1;

// // while (true) {
// // 	if (target === guess) {
// // 		break;
// // 	}
// // 	console.log(`TARGET: ${target} | GUESS: ${guess}`);
// // 	guess = Math.floor(Math.random() * 10) + 1;
// // }

// // console.log(`FINAL TARGET: ${target} | GUESS: ${guess}`);

// // const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// // // for (let i = 0; i < nums.length; i++) {
// // // 	console.log(nums[i]);
// // // }

// // for (let num of nums) {
// // 	console.log(num);
// // }

// // const movies = [
// // 	'harry potter',
// // 	'avengers',
// // 	'justice league',
// // 	'back to the future',
// // 	'inception',
// // 	'interstellar',
// // 	'oppenheimer',
// // ];

// // for (let movie of movies) {
// // 	console.log(movie);
// // }

// // for (let char of 'hello world') {
// // 	console.log(char);
// // }

// // const matrix = [
// // 	[1, 4, 7],
// // 	[9, 7, 2],
// // 	[9, 4, 6],
// // ];

// // for (let row of matrix) {
// // 	for (let col of row) {
// // 		console.log(col);
// // 	}
// // }

// // const cats = ['fashion', 'mobiles', 'books'];
// // const prods = ['tshirt', 'samsung', '1984'];

// // for (let i = 0; i < cats.length; i++) {
// // 	console.log(cats[i], prods[i]);
// // }

// const productPrices = {
// 	Apple: 80000,
// 	OnePlus: 50000,
// 	Samsung: 90000,
// };

// for (let key of Object.keys(productPrices)) {
// 	console.log(key, productPrices[key]);
// }

// for (let key in productPrices) {
// 	console.log(key, productPrices[key]);
// }

// // for (let item of productPrices) {
// // 	console.log(item);
// // }

// // for (let key of Object.keys(productPrices)) {
// // 	console.log(key);
// // }

// // for (let value of Object.values(productPrices)) {
// // 	console.log(value);
// // }

const movieRating = {
	pursuitOfHappiness: 4.8,
	satya: 4.8,
	gangsOfWasepur: 4,
	robot: -3,
};

for (let movie in movieRating) {
	// console.log(movie, movieRating[movie]);
	console.log(`${movie} has a rating of ${movieRating[movie]}`);
}
