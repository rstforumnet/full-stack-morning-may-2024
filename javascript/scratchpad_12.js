// // // const names = ['john', 'jack', 'jane', 'james'];

// // // const upperNames = names.map(function (name) {
// // // 	return name.toUpperCase();
// // // });

// // // // for (let name of names) {
// // // // 	upperNames.push(name.toUpperCase());
// // // // }

// // // console.log(upperNames);

// // const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// // // const doubles = nums.map(function (num) {
// // // 	return num * 2;
// // // });

// // const doubles = nums.map(function (num) {
// // 	return { num: num, isEven: num % 2 === 0 };
// // });

// // console.log(doubles);

// // const add = function (a, b) {
// // 	return a + b;
// // }

// // const add = (a, b) => {
// // 	return a + b;
// // }

// // console.log(add(10, 5))

// // const square = n => n ** 2;

// // console.log(square(10))

// // const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// // const doubles = nums.map((num) => num * 2);

// // // console.log(doubles);

// // let movies = ['The Terminator', 'The Avengers', 'Jurassic Park', 'Titanic'];

// // // const result = movies.find((movie) => movie[0] === 'J');
// // // const result = movies.find((movie) => movie.includes('The'));

// // console.log(result);

// const books = [
// 	{
// 		title: 'The Shining',
// 		author: 'Stephen King',
// 		rating: 4.1,
// 	},
// 	{
// 		title: 'Sacred Games',
// 		author: 'Vikram Chandra',
// 		rating: 4.5,
// 	},
// 	{
// 		title: '1984',
// 		author: 'George Orwell',
// 		rating: 4.9,
// 	},
// 	{
// 		title: 'The Alchemist',
// 		author: 'Paulo Coelho',
// 		rating: 3.5,
// 	},
// 	{
// 		title: 'The Great Gatsby',
// 		author: 'F. Scott Fitzgerald',
// 		rating: 3.8,
// 	},
// ];

// const result = books.filter((book) => book.rating >= 14);
// console.log(result);

// const names = ['jack', 'james', 'john', 'jane', 'josh', 'xrad'];

// // const result = names.every((name) => name[0] === 'j');
// // console.log(result);

// const result = names.some((name) => name[0] === 'b');
// console.log(result);

// const prices = [500.4, 211, 23, 5, 4, 22.2, -23.2, 9233];

// const result = prices.sort((a, b) => b - a);

// console.log(prices);

// const nums = [1, 2, 3, 4, 5];

// const result = nums.reduce((acc, currVal) => acc * currVal);
// console.log(result);

// // acc 		currVal
// // 1			2
// // 3			3
// // 6			4
// // 10			5
// // 15

// let nums = [21, 221, 2, 1, 34, 123, 4342, 56, 4];

// const maxNum = nums.reduce((acc, currVal) => {
// 	if (currVal > acc) {
// 		return currVal;
// 	}
// 	return acc;
// });

// console.log(maxNum);

// acc    currVal
// 21			221
// 221		2
// 221		1
// 221		34
// 221		123
// 221		4342
// 4342

const nums = [1, 2, 3, 4, 5];

const result = nums.reduce((acc, currVal) => acc + currVal, 100);
console.log(result);
