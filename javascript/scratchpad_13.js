// // // // // // function add(a = 0, b = 0) {
// // // // // // 	return a + b;
// // // // // // }

// // // // // // console.log(add());

// // // // // // const nums = [10, 20, 30, 2, 12, 3, 423432, 2323];

// // // // // // console.log(Math.max(...nums));

// // // // // // function dummy(a, b, c) {
// // // // // // 	console.log(a);
// // // // // // 	console.log(b);
// // // // // // 	console.log(c);
// // // // // // }

// // // // // // const data = ['John', 'Jack', 'Jane'];

// // // // // // dummy(...data);

// // // // // const nums1 = [1, 2, 3];
// // // // // const nums2 = [10, 11, 12];

// // // // // // console.log(nums1.concat(nums2));

// // // // // // console.log([...nums1, ...nums2, ...'hello']);

// // // // // console.log('hello'.split(''));
// // // // // console.log([...'hello', 'asjdnasjd', 'asdjkasdjkasdhbfjnk']);

// // // // const moderator = { canEdit: true, authority: 5 };
// // // // const user = { canView: true, authority: 2.5 };

// // // // // console.log({ ...moderator, ...user });

// // // // console.log({ ...'hello world' });

// // // // function add(a, b, ...nums) {
// // // // 	console.log(a);
// // // // 	console.log(b);
// // // // 	console.log(nums);
// // // // }

// // // // add(10, 20, 30, 40, 50, 100, 200);

// // // // function add(...nums) {
// // // // 	return nums.reduce((acc, currVal) => acc + currVal);
// // // // }

// // // // console.log(add(10, 20, 30, 40, 50));

// // // // const users = ['john', 'jane', 'jack'];

// // // // // const admin = users[0];
// // // // // const user = users[1];
// // // // // const mod = users[2];

// // // // const [admin, , mod] = users;

// // // // console.log(admin, mod);

// // // const user = {
// // // 	firstName: 'John',
// // // 	lastName: 'Doe',
// // // 	email: 'john.doe@gmail.com',
// // // 	phone: 99982234567,
// // // };

// // // const { firstName, lastName, ...others } = user;

// // // console.log(firstName, lastName, others);

// // // // const firstName = user.firstName;
// // // // const lastName = user.lastName;
// // // // const emailAddress = user.email;
// // // // const phone = user.phone;

// // function profile({ firstName, lastName, age }) {
// // 	console.log(
// // 		`Hello my name is ${firstName} ${lastName} and I am ${age} years old.`
// // 	);
// // }

// // profile({ firstName: 'Jack', lastName: 'Smith', age: 20 });

// // const movieReviews = [4.5, 5.0, 3.2, 2.1, 4.7, 3.8, 3.1, 3.9, 4.4];
// // const highest = Math.max(...movieReviews);
// // const lowest = Math.min(...movieReviews);

// // let total = 0;
// // movieReviews.forEach((rating) => (total += rating));
// // const average = total / movieReviews.length;

// // const info = { highest, lowest, average, pi: 3.14 };

// // console.log(info);

// const username = 'janedoe';
// const role = 'admin';

// const user1 = { [1 + 2 + 3]: username };

// console.log(user1);

const addProperty = (obj, k, v) => {
	return { ...obj, [k]: v };
};

console.log(addProperty({ firstName: 'John' }, 'lastName', 'Smith'));
