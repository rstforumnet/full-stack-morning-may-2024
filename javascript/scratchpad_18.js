// // // console.log('The first log');
// // // alert('Hello world');
// // // console.log('The second log');

// // // console.log('The first log');
// // // setTimeout(() => {
// // // 	console.log('Hello world');
// // // }, 5000);
// // // console.log('The second log');

// // const btn = document.querySelector('button');

// // setTimeout(() => {
// // 	btn.style.transform = `translateX(100px)`;
// // 	setTimeout(() => {
// // 		btn.style.transform = `translateX(200px)`;
// // 		setTimeout(() => {
// // 			btn.style.transform = `translateX(300px)`;
// // 			setTimeout(() => {
// // 				btn.style.transform = `translateX(400px)`;
// // 				setTimeout(() => {
// // 					btn.style.transform = `translateX(500px)`;
// // 					setTimeout(() => {
// // 						btn.style.transform = `translateX(600px)`;
// // 						setTimeout(() => {
// // 							btn.style.transform = `translateX(700px)`;
// // 						}, 1000);
// // 					}, 1000);
// // 				}, 1000);
// // 			}, 1000);
// // 		}, 1000);
// // 	}, 1000);
// // }, 1000);

// const willGetAPlayStation = new Promise((resolve, reject) => {
// 	const random = Math.random();

// 	if (random > 0.5) {
// 		resolve();
// 	} else {
// 		reject();
// 	}
// });

// willGetAPlayStation
// 	.then(() => {
// 		console.log('Thank you uncle');
// 	})
// 	.catch(() => {
// 		console.log('F you uncle');
// 	});

// const makePlayStationPromise = () => {
// 	return new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			const random = Math.random();
// 			if (random > 0.5) {
// 				resolve();
// 			} else {
// 				reject();
// 			}
// 		}, 5000);
// 	});
// };

// const result = makePlayStationPromise();
// result
// 	.then(() => {
// 		console.log('Promise was resovled');
// 	})
// 	.catch(() => {
// 		console.log('Promise was rejected');
// 	});

function fakeRequest(url) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			const pages = {
				'/users': [
					{ id: 1, username: 'john' },
					{ id: 2, username: 'jane' },
				],
				'/about': 'This is the about page',
				'/users/1': {
					id: 1,
					username: 'johndoe',
					topPostId: 53231,
					city: 'mumbai',
				},
				'/users/5': {
					id: 1,
					username: 'janedoe',
					topPostId: 32443,
					city: 'pune',
				},
				'/posts/53231': {
					id: 1,
					title: 'Really amazing post',
					slug: 'really-amazing-post',
				},
			};

			const data = pages[url];

			if (data) {
				resolve({ status: 200, data });
			} else {
				reject({ status: 404 });
			}
		}, 1000);
	});
}

// fakeRequest('/users')
// 	.then((response) => {
// 		const userId = response.data[0].id;

// 		fakeRequest(`/users/${userId}`)
// 			.then((response) => {
// 				const topPostId = response.data.topPostId;

// 				fakeRequest(`/posts/${topPostId}`)
// 					.then((response) => {
// 						console.log(response);
// 					})
// 					.catch((err) => {
// 						console.log(err);
// 					});
// 			})
// 			.catch((err) => {
// 				console.log(err);
// 			});
// 	})
// 	.catch((err) => {
// 		console.log('REJECTED', err);
// 	});

fakeRequest('/users')
	.then((response) => {
		const userId = response.data[0].id;
		return fakeRequest(`/users/${userId}`);
	})
	.then((response) => {
		const topPostId = response.data.topPostId;
		return fakeRequest(`/posts/${topPostId}`);
	})
	.then((response) => {
		console.log(response);
	})
	.catch((err) => {
		console.log('REJECTED', err);
	});
