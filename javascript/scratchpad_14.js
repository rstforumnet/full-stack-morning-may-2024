// // // // // const math = {
// // // // // 	add(a, b) {
// // // // // 		return a + b;
// // // // // 	},
// // // // // 	sub(a, b) {
// // // // // 		return a - b;
// // // // // 	},
// // // // // 	mul(a, b) {
// // // // // 		return a * b;
// // // // // 	},
// // // // // 	div(a, b) {
// // // // // 		return a / b;
// // // // // 	},
// // // // // };

// // // // // console.log(math.add(10, 20));

// // // // // console.log('Hello World');

// // // // function greet() {
// // // // 	console.log(this);
// // // // }

// // // // greet();

// // // // const aUser99 = {
// // // // 	firstName: 'John',
// // // // 	lastName: 'Doe',
// // // // 	age: 20,
// // // // 	greet() {
// // // // 		console.log(
// // // // 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// // // // 		);
// // // // 	},
// // // // };

// // // // aUser99.greet();

// // // // let aaaahello = 'hello world';

// // // // console.log(this);

// // // // function greet() {
// // // // 	console.log(
// // // // 		`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// // // // 	);
// // // // }

// // // // const user1 = {
// // // // 	firstName: 'Jane',
// // // // 	lastName: 'Doe',
// // // // 	age: 20,
// // // // 	greet,
// // // // };

// // // // const user2 = {
// // // // 	firstName: 'Jack',
// // // // 	lastName: 'Smith',
// // // // 	age: 26,
// // // // 	greet,
// // // // };

// // // // user2.greet();
// // // // user1.greet();

// // // const user = {
// // // 	firstName: 'Jack',
// // // 	lastName: 'Smith',
// // // 	age: 26,
// // // 	greet() {
// // // 		console.log(
// // // 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// // // 		);
// // // 	},
// // // };

// // // let userGreet = user.greet;
// // // userGreet();

// const hellos = {
// 	messages: [
// 		'hello world',
// 		'hello universe',
// 		'hello darkness',
// 		'hello hello',
// 		'heylo',
// 	],
// 	pickMsg: function () {
// 		const index = Math.floor(Math.random() * this.messages.length);
// 		return this.messages[index];
// 	},
// 	start: function () {
// 		setInterval(() => {
// 			console.log(this.pickMsg());
// 		}, 1000);
// 	},
// };

// hellos.start();

// // const user = {
// // 	firstName: 'John',
// // 	lastName: 'Doe',
// // 	age: 20,
// // 	greet: () => {
// // 		// this = window
// // 		console.log(
// // 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// // 		);
// // 	},
// // };

// // user.greet();
